############################################################
# Dockerfile to build Zend Framework2 and LAMP Installed Containers
# Based on Ubuntu
############################################################

# Set the base image to Ubuntu
FROM p7hb/docker-spark:latest

# Install required packages
RUN apt-get clean all
RUN apt-get update
RUN apt-get install -y git --assume-yes apt-utils

#RUN apt-get -y install apache2
#RUN apt-get -y install php5 libapache2-mod-php5 php5-mysql php5-gd php-pear php-apc php5-curl curl lynx-cur
# RUN apt-get -y install git vim

# Add shell scripts for starting apache2
#ADD apache2-start.sh /apache2-start.sh

# Give the execution permissions
#RUN chmod 755 /*.sh

# Add the Configurations files
#ADD conf/supervisord-lamp.conf /etc/supervisor/conf.d/supervisord-lamp.conf

# Enable apache mods.
#RUN a2enmod php5
#RUN a2enmod rewrite

# Adding the Configuration File for host
#ADD conf/000-default.conf /etc/apache2/sites-enabled/000-default.conf

# Retrieve and Installing ZF2
# With gitHub
#RUN mkdir conf
#ADD conf/docker.conf conf/docker.conf
#ADD git.sh /git.sh

#RUN chmod 755 /*.sh

#RUN sh git.sh conf/docker.conf

#RUN cd /var/www/ZendSkeletonApplication

#ADD conf/composer.json .

#RUN curl -sS https://getcomposer.org/installer | php
#RUN php composer.phar install

# Set the port
#EXPOSE 80

#VOLUME ["/var/log/apache2","/etc/php5/apache2/"]

#CMD ["/bin/bash", "/apache2-start.sh"]
